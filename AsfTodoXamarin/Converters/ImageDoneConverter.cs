﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace AsfTodoXamarin.Converters
{
	public class ImageDoneConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (bool)value ? "item_selected.png" : "item_unselected.png";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value;
        }
    }
}
