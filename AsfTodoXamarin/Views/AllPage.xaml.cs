﻿using AsfTodoXamarin.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AsfTodoXamarin.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AllPage : ContentPage
    {
        private readonly AllViewModel viewModel;

        public AllPage()
        {
            InitializeComponent();
            BindingContext = viewModel = new AllViewModel();
        }
    }
}
