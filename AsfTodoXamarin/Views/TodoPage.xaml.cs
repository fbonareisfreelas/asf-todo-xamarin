﻿
using AsfTodoXamarin.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AsfTodoXamarin.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TodoPage : ContentPage
    {
        private readonly TodoViewModel viewModel;

        public TodoPage()
        {
            InitializeComponent();
            BindingContext = viewModel = new TodoViewModel(this.Navigation);
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            if (viewModel.Items.Count == 0)
                viewModel.LoadItemsCommand.Execute(null);
        }
    }
}
