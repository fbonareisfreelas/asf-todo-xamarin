﻿
using AsfTodoXamarin.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AsfTodoXamarin.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DonePage : ContentPage
    {
        private readonly DoneViewModel viewModel;

        public DonePage()
        {
            InitializeComponent();
            BindingContext = viewModel = new DoneViewModel();
        }
    }
}
