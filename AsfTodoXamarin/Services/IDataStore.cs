﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace AsfTodoXamarin.Services
{
    public interface IDataStore<T>
    {
        Task<bool> AddItemAsync(string description);
        Task<bool> UpdateItemAsync(T item);
        Task<bool> DeleteItemAsync(int id);
        Task<T> GetItemAsync(int id);
        Task<IEnumerable<T>> GetItemsAsync(bool forceRefresh = false);
    }
}
