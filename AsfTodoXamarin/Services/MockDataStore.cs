﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AsfTodoXamarin.Models;

namespace AsfTodoXamarin.Services
{
    public class MockDataStore : IDataStore<Todo>
    {
        List<Todo> items;

        public MockDataStore()
        {
            items = new List<Todo>();
            var mockItems = new List<Todo>
            {
                new Todo { Id = 1, Description="This is an item description.", Inclusion = DateTime.Now, IsDone = false },
                new Todo { Id = 2, Description="This is an item description.", Inclusion = DateTime.Now, IsDone = false },
                new Todo { Id = 3, Description="This is an item description.", Inclusion = DateTime.Now, IsDone = true },
                new Todo { Id = 4, Description="This is an item description.", Inclusion = DateTime.Now, IsDone = false },
                new Todo { Id = 5, Description="This is an item description.", Inclusion = DateTime.Now, IsDone = true }
            };

            foreach (var item in mockItems)
            {
                items.Add(item);
            }
        }

        public async Task<bool> AddItemAsync(string description)
        {
            items.Add(new Todo
            {
                Description = description,
                Inclusion = DateTime.Now,
                IsDone = false
            });

            return await Task.FromResult(true);
        }

        public async Task<bool> UpdateItemAsync(Todo item)
        {
            var oldItem = items.FirstOrDefault((Todo arg) => arg.Id == item.Id);
            items.Remove(oldItem);
            items.Add(item);

            return await Task.FromResult(true);
        }

        public async Task<bool> DeleteItemAsync(int id)
        {
            items.Remove(items.FirstOrDefault((Todo arg) => arg.Id == id));
            return await Task.FromResult(true);
        }

        public async Task<Todo> GetItemAsync(int id)
        {
            return await Task.FromResult(items.FirstOrDefault(s => s.Id == id));
        }

        public async Task<IEnumerable<Todo>> GetItemsAsync(bool forceRefresh = false)
        {
            return await Task.FromResult(items);
        }
    }
}