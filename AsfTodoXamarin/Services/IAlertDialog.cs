﻿using System;

namespace AsfTodoXamarin.Services
{
    public interface IAlertDialog
    {
        string Show(Action<string> callback);
    }
}
