﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using AsfTodoXamarin.Views;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace AsfTodoXamarin
{
    public partial class App : Application
    {

        public App()
        {
            InitializeComponent();


            MainPage = new MainPage();
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
