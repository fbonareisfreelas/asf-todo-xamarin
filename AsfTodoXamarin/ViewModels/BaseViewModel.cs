﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;

using Xamarin.Forms;

using AsfTodoXamarin.Models;
using AsfTodoXamarin.Services;
using System.Threading.Tasks;

namespace AsfTodoXamarin.ViewModels
{
    public class BaseViewModel : INotifyPropertyChanged
    {
        public INavigation _navigation;
        public IDataStore<Todo> DataStore => DependencyService.Get<IDataStore<Todo>>() ?? new MockDataStore();
        public IAlertDialog AlertDialog => DependencyService.Get<IAlertDialog>();

        bool isBusy = false;
        public bool IsBusy
        {
            get { return isBusy; }
            set { SetProperty(ref isBusy, value); }
        }

        string title = string.Empty;
        public string Title
        {
            get { return title; }
            set { SetProperty(ref title, value); }
        }

        protected bool SetProperty<T>(ref T backingStore, T value,
            [CallerMemberName]string propertyName = "",
            Action onChanged = null)
        {
            if (EqualityComparer<T>.Default.Equals(backingStore, value))
                return false;

            backingStore = value;
            onChanged?.Invoke();
            OnPropertyChanged(propertyName);
            return true;
        }

        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            var changed = PropertyChanged;
            if (changed == null)
                return;

            changed.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion


        public static Task<string> InputBox(INavigation navigation)
        {
            var tcs = new TaskCompletionSource<string>();

            var lblTitle = new Label { 
                Text = "Title", 
                HorizontalOptions = LayoutOptions.Center, 
                FontAttributes = FontAttributes.Bold 
            };

            var lblMessage = new Label { 
                Text = "Enter new text:" 
            };

            var txtInput = new Entry { 
                Text = "" 
            };

            var btnOk = new Button
            {
                Text = "Ok",
                WidthRequest = 100,
                BackgroundColor = Color.FromRgb(0.8, 0.8, 0.8),
            };

            btnOk.Clicked += async (s, e) =>
            {
                var result = txtInput.Text;
                await navigation.PopModalAsync();
                tcs.SetResult(result);
            };

            var btnCancel = new Button
            {
                Text = "Cancel",
                WidthRequest = 100,
                BackgroundColor = Color.FromRgb(0.8, 0.8, 0.8)
            };

            btnCancel.Clicked += async (s, e) =>
            {
                await navigation.PopModalAsync();
                // pass empty result
                tcs.SetResult(null);
            };

            var slButtons = new StackLayout
            {
                Orientation = StackOrientation.Horizontal,
                Children = { btnOk, btnCancel },
            };

            var layout = new StackLayout
            {
                Padding = new Thickness(0, 40, 0, 0),
                VerticalOptions = LayoutOptions.StartAndExpand,
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                Orientation = StackOrientation.Vertical,
                Children = { lblTitle, lblMessage, txtInput, slButtons },
            };

            // create and show page
            var page = new ContentPage();
            page.Content = layout;
            navigation.PushModalAsync(page);
            // open keyboard
            txtInput.Focus();

            // code is waiting her, until result is passed with tcs.SetResult() in btn-Clicked
            // then proc returns the result
            return tcs.Task;
        }
    }
}
