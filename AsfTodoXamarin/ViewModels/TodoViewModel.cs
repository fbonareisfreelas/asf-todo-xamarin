﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;
using AsfTodoXamarin.Models;
using Xamarin.Forms;

namespace AsfTodoXamarin.ViewModels
{
    public class TodoViewModel : BaseViewModel
    {
        public ObservableCollection<Todo> Items { get; set; }
        public Command LoadItemsCommand { get; set; }
        public Command NewTodoCommand { get; set; }
        public Command UpdateTodoCommand { get; set; }

        public TodoViewModel(INavigation navigation)
        {
            _navigation = navigation;

            Title = "A Fazer";
            Items = new ObservableCollection<Todo>();
            LoadItemsCommand = new Command(async () => await ExecuteLoadItemsCommand());
            NewTodoCommand = new Command(() => ExecuteNewTodoCommand());
            UpdateTodoCommand = new Command(() => ExecuteUpdateTodoCommand(1));
        }

        private void ExecuteNewTodoCommand()
        {
            AlertDialog.Show(async (text) =>
            {
                await DataStore.AddItemAsync(text);
                LoadItemsCommand.Execute(null);
            });
        }

        private void ExecuteUpdateTodoCommand(int id)
        {
            var action = Application.Current.MainPage.DisplayActionSheet(
                "O que deseja fazer?", "Cancelar", null, "Remover", "Marcar como Feita");

            switch (action.Id)
            {
                case 0:
                    DataStore.DeleteItemAsync(id);
                    break;

                case 1:
                    DataStore.UpdateItemAsync(new Todo { IsDone = true });
                    break;
            }

            LoadItemsCommand.Execute(null);
        }

        private async Task ExecuteLoadItemsCommand()
        {
            if (IsBusy)
                return;

            IsBusy = true;

            try
            {
                Items.Clear();
                var items = await DataStore.GetItemsAsync(true);
                foreach (var item in items)
                {
                    Items.Add(item);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}
