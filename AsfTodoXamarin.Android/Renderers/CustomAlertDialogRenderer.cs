﻿using System;
using System.Threading.Tasks;
using Android.App;
using Android.Content.Res;
using Android.Views;
using Android.Widget;
using AsfTodoXamarin.Droid.Renderers;
using AsfTodoXamarin.Services;
using Plugin.CurrentActivity;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: Dependency(typeof(CustomAlertDialogRenderer))]
namespace AsfTodoXamarin.Droid.Renderers
{
    public class CustomAlertDialogRenderer : IAlertDialog
    {
        EditText input;
         
        string IAlertDialog.Show(Action<string> callback)
        {
            var activity = CrossCurrentActivity.Current.Activity as FormsAppCompatActivity;

            AlertDialog.Builder builder = new AlertDialog.Builder(activity);

            builder.SetTitle("Nova Tarefa");
            builder.SetMessage("Qual vai ser o íncrivel nome da sua tarefa?");

            input = new EditText(activity)
            {
                InputType = Android.Text.InputTypes.TextFlagMultiLine
            };

            var container = new FrameLayout(activity);

            FrameLayout.LayoutParams _params = new FrameLayout.LayoutParams(
                ViewGroup.LayoutParams.MatchParent,
                ViewGroup.LayoutParams.WrapContent
            )
            {
                LeftMargin = activity.Resources.GetDimensionPixelSize(Resource.Dimension.dialog_margin),
                RightMargin = activity.Resources.GetDimensionPixelSize(Resource.Dimension.dialog_margin)
            };

            input.LayoutParameters = _params;
            container.AddView(input);

            builder.SetPositiveButton("Adicionar", (sender, e) => {

                callback.Invoke(input.Text);

            });

            builder.SetView(container);
            builder.Show();

            return "a";
        }
    }
}
